# centos-nodejs-mini

A Docker image based on CentOS including a minimal NodeJS install.

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the docker image: `make build`
* Try the docker image: `make shell`
